//
//  GradientBackground.swift
//  Red Dot
//
//  Created by Ziad Ali on 8/3/16.
//  Copyright © 2016 ZiadCorp. All rights reserved.
//

import Foundation
import UIKit

class GradientBackground
{
    static func createGradientLayer(mainView:UIView, index:UInt32)
    {
        let gradientLayer = CAGradientLayer()
        let colorArray:[CGColor] = []
        
        gradientLayer.frame = mainView.bounds
        gradientLayer.colors = colorArray
        mainView.layer.insertSublayer(gradientLayer, atIndex: index)
    }
}
