//
//  OptionsController.swift
//  Red Dot
//
//  Created by Ziad Ali on 7/5/16.
//  Copyright © 2016 ZiadCorp. All rights reserved.
//

import UIKit

class OptionsController: UIViewController
{
    @IBOutlet var gameSoundsSwitch: UISwitch!
    @IBOutlet var gameMusicSwitch: UISwitch!
    @IBOutlet var gameSoundsSlider: UISlider!
    @IBOutlet var gameMusicSlider: UISlider!
    
    
    override func viewWillAppear(animated: Bool)
    {
        gameSoundsSwitch.setOn(soundsOn, animated: false)
        gameMusicSwitch.setOn(musicOn, animated: false)
        
        gameSoundsSlider.setValue(soundsVolume, animated: false)
        gameMusicSlider.setValue(musicVolume, animated: false)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        GradientBackground.createGradientLayer(self.view, index: 0)
    }
        
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func gameSoundsSwitched(sender: AnyObject)
    {
        soundsOn = gameSoundsSwitch.on
    }
    
    @IBAction func gameMusicSwitched(sender: AnyObject)
    {
        musicOn = gameMusicSwitch.on
        if musicOn
        {
            musicPlayer.play()
        }
        else
        {
            musicPlayer.stop()
        }
    }
    
    @IBAction func gameSoundsVolumeAdjusted(sender: AnyObject)
    {
        soundsVolume = gameSoundsSlider.value
    }
    
    @IBAction func gameMusicVolumeAdjusted(sender: AnyObject)
    {
        musicVolume = gameMusicSlider.value
        musicPlayer.volume = musicVolume
    }
}
