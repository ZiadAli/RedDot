//
//  GameCenter.swift
//  Red Dot
//
//  Created by Ziad Ali on 8/22/16.
//  Copyright © 2016 ZiadCorp. All rights reserved.
//

import UIKit
import GameKit

class GameCenter
{
    //initiate gamecenter
    static func authenticateLocalPlayer(view: AnyObject)
    {
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            
            if (viewController != nil)
            {
                view.presentViewController(viewController!, animated: true, completion: nil)
            }
            else
            {
                print((GKLocalPlayer.localPlayer().authenticated))
            }
        }
    }
    
    //Save score
    static func saveHighscore(id:String, score:Int, isFloat:Bool, floatScore:Float)
    {
        print("Saving score")
        //check if user is signed in
        if GKLocalPlayer.localPlayer().authenticated
        {
            print("User is signed in")
            let scoreReporter = GKScore(leaderboardIdentifier: id) //leaderboard id here
            scoreReporter.value = Int64(score)
            if isFloat {scoreReporter.value = Int64(floatScore*100.0)}
            let scoreArray: [GKScore] = [scoreReporter]
            
            GKScore.reportScores(scoreArray, withCompletionHandler: { (error) in
                if error != nil {
                    print("error")
                }
                else
                {
                    print("Success saving score")
                }
            })
        }
    }
    
    /*//shows leaderboard screen
    static func showLeader(view: AnyObject, delegate: GKGameCenterControllerDelegate)
    {
        let vc = view.view?.window?.rootViewController
        let gc = GKGameCenterViewController()
        gc.gameCenterDelegate = delegate
        vc?.presentViewController(gc, animated: true, completion: nil)
    }*/
}
